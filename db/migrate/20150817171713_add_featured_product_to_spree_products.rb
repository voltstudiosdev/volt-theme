class AddFeaturedProductToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :featured_products, :boolean, default: false
  end
end
