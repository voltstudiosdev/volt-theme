Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  get '/players', to: 'players#players'
  get '/privacy', to: 'privacy#index'
end
