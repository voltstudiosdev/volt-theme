Spree::ProductsController.class_eval do
  def show
    @variants = @product.variants_including_master.active(current_currency).includes([:option_values, :images])
    @product_properties = @product.product_properties.includes(:property)
    @tags = @product.taxons.where(parent: Spree::Taxon.where(name: 'Tags'))
    @taxon = Spree::Taxon.find(params[:taxon_id]) if params[:taxon_id]
    @types = @product.option_types
    @in_stock = @product.total_on_hand >= 1 
    
    @cat_dictionary = []
    
    @variants.each do | variant |
      values_ids = []
      
      variant.option_values.each do | value |
        values_ids.push value.id
      end
      
      variant_ids = { variant: variant.id, opt_values: values_ids, price: variant.price.to_s }
      
      @cat_dictionary.push variant_ids
    end
  end
end