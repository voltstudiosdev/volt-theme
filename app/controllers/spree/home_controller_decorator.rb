Spree::HomeController.class_eval do
  def index
    # featured_taxons = [25]
    # taxons = Spree::Taxon.find(featured_taxons)
    
    category = Spree::Taxonomy.where(name: "Category").first
    taxons = category.taxons.where.not(name: category.name)
    
    @featured_categories = []
    taxons.each do |taxon|
      @featured_categories.push({
        name: taxon.name,
        products: taxon.products.where(featured_products: true).where(['available_on < ?', DateTime.now]).limit(3)
      })
    end
    
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    @gift_card = Spree::Product.where(name: "Gift Card").first
  end
end