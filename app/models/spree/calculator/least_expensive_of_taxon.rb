require_dependency 'spree/calculator'

module Spree
  class Calculator::LeastExpensiveOfTaxon < Calculator
    preference :percent_discount, :decimal, default: 50
    preference :amount_of_items, :integer, default: 2
    preference :currency, :string, default: ->{ Spree::Config[:currency] }
    preference :taxon_name, :string, default: "Playeras"

    def self.description
      Spree.t(:least_expensive_of_taxons)
    end

    def compute(object=nil)
      if object && preferred_currency.upcase == object.currency.upcase
        line_items = object.line_items.includes(product: [:taxons]).order(price: :asc)
        
        items_to_discount = line_items.sum(:quantity) / preferred_amount_of_items
        items = []
        discount = 0
        taxon_id = Spree::Taxon.where(name: preferred_taxon_name).pluck(:id).first
        
        line_items.each do |line_item|
          if line_item.product.taxons.pluck(:id).to_a.include? taxon_id
            items << {id: line_item.product.id, quantity: line_item.quantity, price: line_item.price}
          end
        end
        
        items.each do |item|
          break if items_to_discount <= 0
          items_to_use = [items_to_discount, item[:quantity]].min
          discount += items_to_use * item[:price] * (preferred_percent_discount / 100.0)
          items_to_discount -= items_to_use
        end
        
        discount
      else
        0
      end
    end
  end
end