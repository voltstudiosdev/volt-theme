Deface::Override.new(
    virtual_path: 'spree/admin/products/_form',
    name: "add_featured_to_product_edit",
    insert_after: "erb[loud]:contains('text_field :price')",
    text: "
    <%= f.field_container :featured_products do %>
      <%= f.label :featured_products, 'Featured' %>
      <%= f.check_box :featured_products, :value => @product.featured_products %>
      <%= f.error_message_on :sale_price %>
    <% end %>
  "
)