Spree.ready ($) ->
  sameElements = (arr1, arr2) ->
    $(arr1).not(arr2).length == 0 and $(arr2).not(arr1).length == 0
    
  formatMoney = (n, t=',', d='.', c='$') ->
    s = if n < 0 then "-#{c}" else c
    i = Math.abs(n).toFixed(2)
    j = (if (j = i.length) > 3 then j % 3 else 0)
    s += i.substr(0, j) + t if j
    return s + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
  
  Spree.addImageHandlers = ->
    thumbnails = ($ '#product-images ul.thumbnails')
    ($ '#main-image').data 'selectedThumb', ($ '#main-image img').attr('src')
    ($ '#main-image').data 'selectedThumbZoom', ($ '#main-image img').attr('data-zoom-image')
    thumbnails.find('li').eq(0).addClass 'selected'
    thumbnails.find('a').on 'click', (event) ->
      ($ '#main-image').data 'selectedThumb', ($ event.currentTarget).attr('href')
      ($ '#main-image').data 'selectedThumbZoom', ($ event.currentTarget).attr('zoom-href')
      ($ '#main-image').data 'selectedThumbId', ($ event.currentTarget).parent().attr('id')
      thumbnails.find('li').removeClass 'selected'
      ($ event.currentTarget).parent('li').addClass 'selected'
      false

    thumbnails.find('li').on 'mouseenter', (event) ->
      ($ '#main-image img').data('elevateZoom').swaptheimage ($ event.currentTarget).find('a').attr('href'), ($ event.currentTarget).find('a').attr('zoom-href')

    thumbnails.find('li').on 'mouseleave', (event) ->
      ($ '#main-image img').data('elevateZoom').swaptheimage ($ '#main-image').data('selectedThumb'), ($ '#main-image').data('selectedThumbZoom')
      
  ($ '#main-image img').elevateZoom
    cursor: "crosshair"

  Spree.showVariantImages = (variantId) ->
    ($ 'li.vtmb').hide()
    ($ 'li.tmb-' + variantId).show()
    currentThumb = ($ '#' + ($ '#main-image').data('selectedThumbId'))
    if not currentThumb.hasClass('vtmb-' + variantId)
      thumb = ($ ($ '#product-images ul.thumbnails li:visible.vtmb').eq(0))
      thumb = ($ ($ '#product-images ul.thumbnails li:visible').eq(0)) unless thumb.length > 0
      newImg = thumb.find('a').attr('href')
      ($ '#product-images ul.thumbnails li').removeClass 'selected'
      thumb.addClass 'selected'
      ($ '#main-image img').attr 'src', newImg
      ($ '#main-image').data 'selectedThumb', newImg
      ($ '#main-image').data 'selectedThumbId', thumb.attr('id')

  Spree.updateVariantPrice = (price) ->
    ($ '.price.selling').text(formatMoney(price))
    
  radios = ($ '#option-values input[type="radio"]')
  
  Spree.getProductVariant = ->
    selectedRadios = ($ '#option-values input[type="radio"]:checked')
    valuesId = (parseInt(option.value) for option in selectedRadios)
    variant = variant for variant in Spree.catDictionary when sameElements(variant.opt_values, valuesId)

  Spree.addImageHandlers()

  radios.click (event) ->
    variant = Spree.getProductVariant()[0]
    if variant
      Spree.showVariantImages variant.variant
      Spree.updateVariantPrice variant.price
      ($ '#variant_id').val(variant.variant)

