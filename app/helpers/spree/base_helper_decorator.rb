module Spree
  module BaseHelper
    private
    
    def create_product_image_tag(image, product, options, style)
      options.reverse_merge! alt: image.alt.blank? ? product.name : image.alt
      if options.has_key? :'data-zoom-image'
        options[:'data-zoom-image'] = image.attachment.url(options[:'data-zoom-image'])
      end
      image_tag image.attachment.url(style), options
    end
  end
end